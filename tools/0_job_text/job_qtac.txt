
# qc
$fastqc \
--outdir $fastqc_path \
--threads 2 $smpl_R1 $smpl_R2

# trim
java -jar $trimmomatic PE \
-phred33 $smpl_R1 $smpl_R2 $trim_pair_R1 $trim_unpair_R1 $trim_pair_R2 $trim_unpair_R2 \
ILLUMINACLIP:/projects/b1036/shmo/rnaseq/tools/2_trim/TruSeq2-PE_SM.fa:2:30:10 \
SLIDINGWINDOW:5:20 \
LEADING:20 \
TRAILING:20 \
MINLEN:75

# align
$star \
--runThreadN 22 \
--readFilesCommand zcat \
--outSAMtype BAM SortedByCoordinate \
--quantMode GeneCounts \
--outReadsUnmapped Fastx \
--outFilterType BySJout \
--genomeDir $star_index \
--sjdbGTFfile $hg19_ref \
--readFilesIn $trim_pair_R1 $trim_pair_R2 \
--outFileNamePrefix $star_pair_all_in \
--outSAMstrandField intronMotif

$star \
--runThreadN 22 \
--readFilesCommand zcat \
--outSAMtype BAM SortedByCoordinate \
--quantMode GeneCounts \
--outReadsUnmapped Fastx \
--outFilterType BySJout \
--genomeDir $star_index \
--sjdbGTFfile $hg19_ref \
--readFilesIn $trim_unpair_R1 \
--outFileNamePrefix $star_unpair_R1_in \
--outSAMstrandField intronMotif

$star \
--runThreadN 22 \
--readFilesCommand zcat \
--outSAMtype BAM SortedByCoordinate \
--quantMode GeneCounts \
--outReadsUnmapped Fastx \
--outFilterType BySJout \
--genomeDir $star_index \
--sjdbGTFfile $hg19_ref \
--readFilesIn $trim_unpair_R2 \
--outFileNamePrefix $star_unpair_R2_in \
--outSAMstrandField intronMotif

# count
htseq-count  \
--format bam \
--order pos \
--strand no \
-q $star_pair_all_out $hg19_ref > $htseq_pair_all

htseq-count  \
--format bam \
--order pos \
--strand no \
-q $star_unpair_R1_out $hg19_ref > $htseq_unpair_R1

htseq-count  \
--format bam \
--order pos \
--strand no \
-q $star_unpair_R2_out $hg19_ref > $htseq_unpair_R2

cd $rnaseq_pipeline
module load anaconda3/2018.12
python ./helpers/htseq_merge.py $smpl $htseq_path
python ./helpers/unlink.py $keep_bam $smpl $trim_path $star_path
