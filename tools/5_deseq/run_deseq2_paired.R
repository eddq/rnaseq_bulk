#!/software/R/2.15.2/bin/Rscript

library(DESeq2,lib.loc="/projects/b1036/SHARED/R_MODULES")
library(data.table)


cts <- read.table("htseq_raw_paired_lapse.txt", sep = "\t", header = TRUE, row.names=1)​
coldata <- read.table("col_data.csv", sep = ",", header = TRUE, row.names = 1)


#make dds object
dds <- DESeqDataSetFromMatrix(countData = cts,
                              colData = coldata,
                              design = ~ patient + condition)
dds

dds <- DESeq(dds)

res <- results(dds)
res

write.csv(counts(dds, normalized=T), file = "normalized_counts.csv", quote = F)
