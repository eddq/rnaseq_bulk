#!/bin/bash
#SBATCH -A b1042                # Allocation
#SBATCH -p genomics             # Queue
#SBATCH -t 48:00:00             # Walltime/duration of the job
#SBATCH -N 1                    # Number of Nodes
#SBATCH -n 12    				# Number of Cores (Processors)
#SBATCH --export=NONE