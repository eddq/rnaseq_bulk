# import std libraries
import pandas as pd
import numpy as np
import click
import yaml
import math
import sys
from pathlib import Path

# import scripts
pipeline_path = 
sys.path.insert(1, pipeline_path)
from helpers import prep, readin, generate


@click.command()
@click.option('--keep_bam', 
              type=click.Choice(['y','n']),
              prompt='Do you want to keep the BAM files after aligning with STAR?',
              help='This option allows you to keep the alignment files (BAM) from STAR.')

def cli(keep_bam):
    generate.jobs.qtac(keep_bam)
    generate.jobs.deseq2()
    generate.jobs.gsva()
    
cli()