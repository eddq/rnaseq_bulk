# import std libraries
import pandas as pd
import numpy as np
import click
from pathlib import Path
import sys

# import scripts
pipeline_path = 
sys.path.insert(1, pipeline_path)
from helpers import prep


@click.command()
@click.argument('keep_bam', type=str, required=True)
@click.argument('smpl', type=str, required=True)
@click.argument('trim_path', type=str, required=True)
@click.argument('star_path', type=str, required=False)


def cli(keep_bam, smpl, trim_path, star_path):

    def unlink_trim(smpl, trim_path):

        # unlink the fastq files post trimming in trim_path
        trim_dir = Path(trim_path)
        trim_files = sorted(trim_dir.glob('{}*'.format(smpl)))
        for file in trim_files:
            print(file)
            file.unlink()
        

    def unlink_star(smpl, star_path):

        # unlink aligned bam files in star_path
        star_dir = Path(star_path)
        extensions = ['{}*.out.bam'.format(smpl), '{}*.out.mate*'.format(smpl)]
        star_bam_files = []

        for ext in extensions:
            star_bam_files.extend(star_dir.glob(ext))

        for file in star_bam_files:
            file.unlink()

    unlink_trim(smpl, trim_path)
    if keep_bam == 'y':
        pass
    elif keep_bam == 'n':
        unlink_star(smpl, star_path)
        

cli()