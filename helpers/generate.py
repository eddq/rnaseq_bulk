import yaml
import sys
import re
import pandas as pd
from pathlib import Path
# Reference to pipeline reference
pipeline_path = 
sys.path.insert(1, pipeline_path)
from helpers import prep, readin


# acquire project information
project_path, \
fastqc_path, \
trim_path, \
star_path, \
htseq_path, \
deseq2_path, \
gsva_path = prep.project_dir.acquire_info()

# Define paths to necessary elements
path_local_yaml_stream = open(Path('{}/config/path_local.yml'.format(pipeline_path)), 'r')
path_quest_yaml_stream = open(Path('{}/config/path_quest.yml'.format(pipeline_path)), 'r')
path_local = yaml.load(path_local_yaml_stream)
path_quest = yaml.load(path_quest_yaml_stream)

# create job directory
job_dir = Path('{}/_jobs'.format(project_path))
job_dir.mkdir(exist_ok=True)


def write_run_all_script(sub_dir):
    
    run_all_script = 'RUN_ALL.sh'
    run_all_path = sub_dir / run_all_script
    with open(run_all_path, 'w') as run_all:
        run_all.write('')
        
    return run_all_path
    

class jobs:
    
    @staticmethod
    def qtac(keep_bam):
        
        qtac_job_dir = job_dir / '1.qtac'
        qtac_job_dir.mkdir(exist_ok=True)
            
        run_all_path = write_run_all_script(qtac_job_dir)
        
        rnaseq_pipeline = pipeline_path
        keep_bam = keep_bam
        fastqc = path_quest['qtac']['fastqc']
        trimmomatic = '{}/'.format(pipeline_path) + path_local['qtac']['trimmomatic']
        star = path_quest['qtac']['star']
        star_index = path_quest['qtac']['star_index']
        hg19_ref = path_quest['qtac']['hg19_ref']
        b1042_template = '{}/'.format(pipeline_path) + path_local['header']['b1042_template']
        qtac_template = '{}/'.format(pipeline_path) + path_local['qtac']['qtac_template']  
        
        
        # readin will be local to the project_dir
        input_fastq = readin.fastq(project_path)
        for smpl in input_fastq:

            ### define sample names ###
            smpl_R1 = input_fastq[smpl][0]
            smpl_R2 = input_fastq[smpl][1]
            # trimmomatic
            trim_pair_R1 = trim_path.as_posix() + '/{}_paired_R1.fq.gz'.format(smpl)
            trim_pair_R2 = trim_path.as_posix() + '/{}_paired_R2.fq.gz'.format(smpl)
            trim_unpair_R1 = trim_path.as_posix() + '/{}_unpaired_R1.fq.gz'.format(smpl)
            trim_unpair_R2 = trim_path.as_posix() + '/{}_unpaired_R2.fq.gz'.format(smpl)
            # star to align
            star_pair_all_in = star_path.as_posix() + '/{}_paired_all_'.format(smpl)
            star_unpair_R1_in = star_path.as_posix() + '/{}_unpaired_R1_'.format(smpl)
            star_unpair_R2_in = star_path.as_posix() + '/{}_unpaired_R2_'.format(smpl)
            
            star_pair_all_out = star_path.as_posix() + '/{}_paired_all_Aligned.sortedByCoord.out.bam'.format(smpl)
            star_unpair_R1_out = star_path.as_posix() + '/{}_unpaired_R1_Aligned.sortedByCoord.out.bam'.format(smpl)
            star_unpair_R2_out = star_path.as_posix() + '/{}_unpaired_R2_Aligned.sortedByCoord.out.bam'.format(smpl)
            # HTseq to count
            htseq_pair_all = htseq_path.as_posix() + '/{}_paired_all.ht.counts'.format(smpl)
            htseq_unpair_R1 = htseq_path.as_posix() + '/{}_unpaired_R1.ht.counts'.format(smpl)
            htseq_unpair_R2 = htseq_path.as_posix() + '/{}_unpaired_R2.ht.counts'.format(smpl)
            
            # Job Variables
            job_vars = ['rnaseq_pipeline','keep_bam','fastqc','trimmomatic','star','star_index','hg19_ref',
                        'smpl','smpl_R1','smpl_R2',
                        'trim_pair_R1','trim_pair_R2','trim_unpair_R1','trim_unpair_R2',
                        'star_pair_all_in','star_unpair_R1_in','star_unpair_R2_in',
                        'star_pair_all_out','star_unpair_R1_out','star_unpair_R2_out',
                        'htseq_pair_all','htseq_unpair_R1','htseq_unpair_R2']            

            # create script name and path
            qtac_job_script = 'qtac_{}.sh'.format(smpl)
            qtac_job_path = qtac_job_dir / qtac_job_script

            with open(qtac_job_path, 'w') as job, \
            open(b1042_template, 'r') as header, \
            open(qtac_template, 'r') as job_qtac:

                job_header = header.readlines()
                job.writelines(job_header)
                job.write('\n#SBATCH -J qtac_{}\n'.format(smpl))
                job.write('cd $SLURM_SUBMIT_DIR\n')
                job.write('module load python\n')
                job.write('module load tophat/2.1.0\n')
                job.write('module load java\n\n')
                
                # write variables
                for job_var in job_vars:
                    job.write('{}={}\n'.format(job_var, locals()[job_var]))
                job.write('fastqc_path={}\n'.format(fastqc_path))
                job.write('trim_path={}\n'.format(trim_path))
                job.write('star_path={}\n'.format(star_path))
                job.write('htseq_path={}\n'.format(htseq_path))
                
                # write the qtac process
                job_qtac_txt = job_qtac.readlines()
                job.writelines(job_qtac_txt)

            with open(run_all_path, 'a') as run_all:
                run_all.write('sbatch {}\n'.format(qtac_job_path))

                
    @staticmethod
    def deseq2():
        
        deseq2_job_dir = job_dir / '2.deseq2'
        deseq2_job_dir.mkdir(exist_ok=True)
        
        run_all_path = write_run_all_script(deseq2_job_dir)
        
        deseq2_group_data = readin.deseq2_groups(project_path)
        deseq2_compare_data = readin.deseq2_compare(project_path)

        for comparison in deseq2_compare_data:

            comparison_dir = deseq2_path / comparison
            comparison_dir.mkdir(exist_ok=True)
            # need to make samples.txt file 
            compare_1 = comparison.split('_')[0]
            compare_2 = comparison.split('_')[1]

            # sample, htseq.merge.count, group
            df = pd.DataFrame(columns=['smpl', 'htseq_count', 'compare'])

            for smpl in deseq2_group_data[compare_1]:
                df_dict = {'smpl':smpl, 'htseq_count':'{}.merge.count'.format(smpl), 'compare':compare_1}
                df_temp = pd.DataFrame(data=df_dict, index=[0])
                df = df.append(df_temp)

            for smpl in deseq2_group_data[compare_2]:
                df_dict = {'smpl':smpl, 'htseq_count':'{}.merge.count'.format(smpl), 'compare':compare_2}
                df_temp = pd.DataFrame(data=df_dict, index=[0])
                df = df.append(df_temp)

            # create samples.txt for comparison groups
            output_path = comparison_dir / 'samples.txt'
            df.to_csv(Path(output_path), sep='\t', index=None, header=False)

            # create deseq script
            # create script name and path
            deseq2_job_script = 'deseq2_{}.sh'.format(comparison)
            deseq2_job_path = deseq2_job_dir / deseq2_job_script
            
            deseq_script = '{}/'.format(pipeline_path) + path_local['deseq2']['deseq_script']
            deseq_perl = '{}/'.format(pipeline_path) + path_local['deseq2']['deseq_perl']
            deseq_misc = '{}/'.format(pipeline_path) + path_local['deseq2']['deseq_misc']
            b1036_template = '{}/'.format(pipeline_path) + path_local['header']['b1036_template']
            deseq2_template = '{}/'.format(pipeline_path) + path_local['deseq2']['deseq2_template']
            
            deseq_result = comparison_dir.as_posix() + '/{}_DESeq2_results.csv'.format(comparison)
            deseq_annotated = comparison_dir.as_posix() + '/{}_DESeq2_annotated.txt'.format(comparison)
            
            job_vars = ['deseq_script','comparison_dir','comparison','compare_2','compare_1',
                        'deseq_perl','deseq_result','deseq_misc','deseq_annotated']
            

            with open(deseq2_job_path, 'w') as job, \
            open(b1036_template, 'r') as header, \
            open(deseq2_template, 'r') as job_deseq2:

                job_header = header.readlines()
                job.writelines(job_header)
                job.write('\n#SBATCH -J deseq2_{}\n'.format(comparison))
                job.write('cd $SLURM_SUBMIT_DIR\n')
                job.write('module load R/3.2.2\n\n')
                
                # write variables
                for job_var in job_vars:
                    job.write('{}={}\n'.format(job_var, locals()[job_var]))
                job.write('htseq_path={}\n'.format(htseq_path))
                
                # write the deseq2 process
                job_deseq2_txt = job_deseq2.readlines()
                job.writelines(job_deseq2_txt)

            with open(run_all_path, 'a') as run_all:
                run_all.write('sbatch {}\n'.format(deseq2_job_script))
 
    @staticmethod
    def gsva():

        gsva_job_dir = job_dir / '3.gsva'
        gsva_job_dir.mkdir(exist_ok=True)
        
        run_all_path = write_run_all_script(gsva_job_dir)
        
        with open(run_all_path, 'w') as run_all:
            run_all.write('')
            
        gsva_geneset_path = Path('{}/'.format(pipeline_path) + path_local['gsva']['gsva_geneset_path'])
        gsva_geneset = sorted(gsva_geneset_path.glob('*.all.v5.1.symbols.gmt'))
        
        for geneset_path in gsva_geneset:
    
            geneset_name = geneset_path.name.split('.')[0]

            deseq2_group_data = readin.deseq2_groups(project_path)
            deseq2_compare_data = readin.deseq2_compare(project_path)

            # create one gsva script for every geneset
            # every gsva script will have multiple R scripts for each comparison group
            gsva_job_script = 'gsva_{}.sh'.format(geneset_name)
            gsva_job_path = gsva_job_dir / gsva_job_script

            # bulk text
            b1036_template = '{}/'.format(pipeline_path) + path_local['header']['b1036_template']
            gsva_template = '{}/'.format(pipeline_path) + path_local['gsva']['gsva_template']
            gsva_script = '{}/'.format(pipeline_path) + path_local['gsva']['gsva_script']

            # job write will go down here since need to do every comparison group
            with open(gsva_job_path, 'w') as job, \
            open(b1036_template, 'r') as header, \
            open(gsva_template, 'r') as job_gsva:

                job_header = header.readlines()
                job.writelines(job_header)
                job.write('\n#SBATCH -J gsva_{}\n'.format(geneset_name))
                job.write('cd $SLURM_SUBMIT_DIR\n')
                job.write('module load R/3.2.2\n\n')
                
                job.write('gsva_script={}\n'.format(gsva_script))
                job.write('geneset_path={}\n\n'.format(geneset_path.as_posix()))
                
                job_gsva_txt = job_gsva.readlines()

                for comparison in deseq2_compare_data:

                    compare_1 = comparison.split('_')[0]
                    compare_2 = comparison.split('_')[1]
                    deseq2_comparison_dir = deseq2_path / comparison
                    deseq2_samples = deseq2_comparison_dir.as_posix() + '/samples.txt'
                    deseq2_norm_count = deseq2_path / comparison / '{}_DESeq2_normalized_counts.csv'.format(comparison)
                    
                    # need to geneset folders in gsva_path
                    gsva_geneset_dir = gsva_path / geneset_name
                    gsva_geneset_dir.mkdir(exist_ok=True)
                    gsva_comparison_result = gsva_geneset_dir.as_posix() + '/{}_gsva.txt'.format(comparison)

                    job.write('deseq2_norm_count={}\n'.format(deseq2_norm_count))
                    job.write('deseq2_samples={}\n'.format(deseq2_samples))
                    job.write('compare_2={}\n'.format(compare_2))
                    job.write('compare_1={}\n'.format(compare_1))
                    job.write('gsva_comparison_result={}\n'.format(gsva_comparison_result))
                    
                    # write the qtac process
                    job.writelines(job_gsva_txt)
                    job.write('\n')

            with open(run_all_path, 'a') as run_all:
                run_all.write('sbatch {}\n'.format(gsva_job_script))